from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home-base'),
    path('contact/', views.contact, name='contact-page'),
    path('project/', views.project, name='project-page')
]